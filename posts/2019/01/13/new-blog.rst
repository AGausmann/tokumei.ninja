.. title: My brand new blog with a brand new engine!
.. slug: new-blog
.. date: 2019-01-13 22:29:01+00:00
.. tags: 
.. category: meta
.. link: 
.. description: 
.. type: text

This is the first post of a great future. My old blog has not been active for a
while, and there was never much content, so it's time for a reboot. This time,
I'm using the `Nikola <https://getnikola.com>`_ static site generator which
seems to be a lot nicer than Pelican (but don't quote me on that).

This will mainly be a place for me to log progress on personal projects and
post writeups for CTF challenges, building up content over time so I can have
something to put on my resume. I'm going to be updating this fairly often, so
if you like what you see, be sure to stay tuned for more!
